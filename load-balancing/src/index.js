var express = require('express');
var os = require("os");

var app = express();
var hostname = os.hostname();

app.get('/', function (req, res) {
    // A CPU Intensive task:
    for (var i = 0; i < 1000000 ; i++) {
        Math.pow(i,i);
        Math.pow(i,i);
    }
    // Respond the request:
    res.send('<html><body>Hello from Node.js container '
        + hostname + '</body></html>');
});

app.listen(80);
console.log('Running on http://localhost');
